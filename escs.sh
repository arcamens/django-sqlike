##############################################################################
cd ~/projects/
git clone git@bitbucket.org:arcamens/sqlike.git sqlike-code
##############################################################################

# push, sqlike, master.
cd ~/projects/django-sqlike-code
# clean up all .pyc files. 
find . -name "*.pyc" -exec rm -f {} \;

git status
git add *
git commit -a
git push -u origin master
##############################################################################
# create dev branch.
cd ~/projects/sqlike-code
git branch -a
git checkout -b development
git push --set-upstream origin development
##############################################################################

# push, sqlike, development.
cd ~/projects/sqlike-code
# clean up all .pyc files. 
find . -name "*.pyc" -exec rm -f {} \;
git status
git add *
git commit -a
git push -u origin development
##############################################################################
# merge, dev, into, master:
git checkout master
git merge development
git push -u origin master
git checkout development
##############################################################################
# sqlike, pull.
cd ~/projects/sqlike-code
git pull
##############################################################################
# delete, last, commit.

cd ~/projects/sqlike-code
git reset HEAD^ --hard
git push -f
##############################################################################
# create, first, release, test, tag.

git tag -a 0.0.1 -m 'Initial release'
git push origin : 0.0.1
##############################################################################
